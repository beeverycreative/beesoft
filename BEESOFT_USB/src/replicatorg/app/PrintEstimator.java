package replicatorg.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Copyright (c) 2013 BEEVC - Electronic Systems This file is part of BEESOFT
 * software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version. BEESOFT is
 * distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details. You
 * should have received a copy of the GNU General Public License along with
 * BEESOFT. If not, see <http://www.gnu.org/licenses/>.
 */
public class PrintEstimator {

    private static String GCODER_PATH = Base.getApplicationDirectory().getAbsolutePath().concat("/estimator/gcoder.py");
    private static String estimatedTime = "ND";
    private static String BIN_PATH_UNIX = "python";
    private static String BIN_PATH_WINDOWS = "C:\\Python27\\python.exe";

    public static String getEstimatedTime() {
        return estimatedTime;
    }
            
    public static String estimateTime(File gcode) {
        String[] command = new String[3];
        
        if(Base.isMacOS() || Base.isLinux())
        {
            command[0] = BIN_PATH_UNIX;
            command[1] = GCODER_PATH;
            command[0] = gcode.getAbsolutePath();
        }
        else
        {
            command[0] = BIN_PATH_WINDOWS;
            command[1] = GCODER_PATH;
            command[0] = gcode.getAbsolutePath();  
        }
    
        ProcessBuilder probuilder = new ProcessBuilder(command);

        //You can set up your work directory
//        probuilder.directory(new File("c:\\xyzwsdemo"));

        Process process = null;
        try {
            process = probuilder.start();
        } catch (IOException ex) {
            Base.writeLog("Error starting process to estimate print duration");
            estimatedTime = "NA";
            return "Error";
        }

        //Read out dir output
        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
        String output = "";
        try {
            while ((line = br.readLine()) != null) {
                output = output.concat(line);
            }
        } catch (IOException ex) {
            Base.writeLog("Error reading gcode estimater output");
        }

        //Wait to get exit value
        try {
            int exitValue = process.waitFor();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
//        System.out.println(output);
        estimatedTime = parseOutput(output);
        return estimatedTime;
    }
    
    private static String parseOutput(String out)
    {
        if(!out.isEmpty())
        {
            String re1=".*?";	// Non-greedy match on filler
            String re2="((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";	// HourMinuteSec 1
            String time1 = "ND";

            Pattern p = Pattern.compile(re1+re2,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
            Matcher m = p.matcher(out);
            if (m.find())
            {
                time1=m.group(1);
//                System.out.print("("+time1.toString()+")"+"\n");
            }
            
            return filterTime(time1.toString());
        }
        return "ND";
    }
    
    private static String filterTime(String baseEstimation)
    {
        String estimation = "ND";
        int hours = 0;
        int minutes = 0;
        int seconds = 0;  
        
        String re1="(\\d+)";	// Integer Number 1
        String re2=".*?";	// Non-greedy match on filler
        String re3="(\\d+)";	// Integer Number 2
        String re4=".*?";	// Non-greedy match on filler
        String re5="(\\d+)";	// Ibasenteger Number 3

        Pattern p = Pattern.compile(re1+re2+re3+re4+re5,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(baseEstimation);
        if (m.find())
        {
            hours = Integer.valueOf(m.group(1));
            minutes = Integer.valueOf(m.group(2));
            seconds = Integer.valueOf(m.group(3));
        }
        
        if(hours > 0)
            return String.valueOf(hours).concat(":").concat(String.valueOf(minutes));
       
        return String.valueOf(minutes);
            
    }
    
    private static String tuneEstimation(String baseEstimation)
    {
        String estimation = "ND";
        int hours = 0;
        int minutes = 0;
        int seconds = 0;  
        
        String re1="(\\d+)";	// Integer Number 1
        String re2=".*?";	// Non-greedy match on filler
        String re3="(\\d+)";	// Integer Number 2
        String re4=".*?";	// Non-greedy match on filler
        String re5="(\\d+)";	// Ibasenteger Number 3

        Pattern p = Pattern.compile(re1+re2+re3+re4+re5,Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(baseEstimation);
        if (m.find())
        {
            hours = Integer.valueOf(m.group(1));
            minutes = Integer.valueOf(m.group(2));
            seconds = Integer.valueOf(m.group(3));
            System.out.print("("+hours+")"+"("+minutes+")"+"("+seconds+")"+"\n");
        }
        
        double multiplier = 0.35;
        hours *= (int) multiplier;
        minutes *= (int) multiplier;
        seconds *= (int) multiplier;
        
        estimation = trunkTime(hours, minutes, seconds);
        
        return estimation;
        
    }
    
    private static String trunkTime(int hours, int minutes, int seconds)
    {
        int hoursTrunked = hours;
        int minutesTrunked = minutes;
        int secondsTrunked = seconds;
        double quocient = 0;
        final int BASE = 60;
        
        if(seconds > BASE)
        {
            quocient = seconds/60;
            int minutesCarry = (int)quocient;
            double fractional = secondsTrunked - minutesCarry;
            
            minutes += minutesCarry;
            secondsTrunked = (int)Math.round(fractional * 60);
        }
        if(minutes > BASE)
        {
            quocient = minutes/60;
            int hoursCarry = (int)quocient;
            
            hoursTrunked += hoursCarry;
            minutesTrunked = (int)Math.round(minutes % BASE);
        }
                    
        return String.valueOf(hoursTrunked).concat(":").concat(String.valueOf(minutesTrunked));
    }
}
